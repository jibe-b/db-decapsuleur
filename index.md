---
title: DB-décapsuleur, pour demander la libération de données produites par les laboratoires de ton université, c'est facile !
...

## Formulaire de demande de libération de données

Les laboratoires de ton université produisent des données qui doivent être mises à disposition en open data une fois le projet fini (hé oui, on appelle ça la loi).

Ces données peuvent te servir pour t'entrainer à l'analyse de données ou pour tes propres projets scientifiques.

Alors pour aller plus loin que les TP où les résultats sont les mêmes depuis des années, profite de ce formulaire pour demander la libération de jeux de données qui peuvent t'intéresser !

[Faire une demande](https://gitlab.com/jibe-b/db-decapsuleur/issues/new)
