# Description d'un jeu de données à demander

Veuillez remplir les entrées suivantes, autant que possible (plus vous en remplirez et plus votre demande sera traitée en priorité) :

- Laboratoire détenteur des données :
- Personne à contacter :
- Description du jeu de données :

Notez que vous recevrez une réponse sur ce fil de discussion:
